<?php

namespace Drupal\slider;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Slider entity.
 *
 * @see \Drupal\slider\Entity\slider.
 */
class sliderAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\slider\Entity\sliderInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished slider entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published slider entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit slider entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete slider entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add slider entities');
  }

}
