## Docker:
#### Docker information:
######Docker images:
* php:7.1-apache
* percona/percona-server
* phpmyadmin/phpmyadmin

#### Docker configuration:

To build docker containers which already setup in docker-compose.yml file, you have to run command in terminal as bellow:  
###### Build container:

    docker-compose up -d
###### List container:

    docker ps
###### Remove container: 
    docker-compose down
    
###### List docker images: 
    docker images
    
###### Remove docker images: 
    docker rmi image_hash_name
    
#### Docker Source code:
All source stored in **src** folder will be mapped to **/var/www/html** folder in docker image (**itconsultis_web**)

PHP configuration file which mapped to container will be stored in **docker/php/config/php.ini**

<br/>

## Drupal Installation:
Version: 8.3.6

######Contribution modules:
* config_rewrite
* devel

######Custom modules:
* blog
* ic_contact
* pages
* slider

### Containers:
####itconsultis_web container:
    Hostname: localhost
    Port: 80
    Web folder: /var/www/html
    
####percona/percona-server container:
    Hostname: percona
    Port: 3306
    Root username: root
    Root password: root
    
####phpmyadmin/phpmyadmin container:
    Hostname: localhost
    Port: 8080
    
### Setup step Guild:
* After build docker-compose completed, run setup drupal 8 by url **_http://localhost_**
* Fullfil site informations
* Login with your administrator's account
* Go to **_admin/appearance_** and find **It-Consultis 8.3.6** theme, then click **_Install and set as default_**
* Go to **_admin/modules_**, find and install modules as bellow:

     * **Devel generate**
     * **IT Consultis Blog**
     * **IT Consultis Contact**
     * **IT Consultis Custom Pages**
     * **IT Consultis Slider**

* To generate blogs content, go to **admin/config/development/generate/content** and choose **Blog** content type and render node entities as form instruction.

* To clear cache, go to **_admin/config/development/performance_**

### Create Slider:
Slider is an entity, i created bundle Banner Slider(banner_slider) for slider content.

* To create slider, go to **admin/structure/slider**, click **Add slider**

Slider content will be show in **banner slider** block which i rendered on Front page  