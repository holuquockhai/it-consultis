<?php
namespace Drupal\pages\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "front_feature",
 *   admin_label = @Translation("Front Feature"),
 *   category = @Translation("Front Feature"),
 * )
 */
class FrontFeatureBlock extends BlockBase {
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    // TODO: Implement build() method.
    return
      [
        '#theme' => 'front_feature_block',
      ];
  }
  
}