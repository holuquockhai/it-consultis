<?php

namespace Drupal\slider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Slider type entities.
 */
interface sliderTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
