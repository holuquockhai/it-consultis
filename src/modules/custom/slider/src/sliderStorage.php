<?php

namespace Drupal\slider;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\slider\Entity\sliderInterface;

/**
 * Defines the storage handler class for Slider entities.
 *
 * This extends the base storage class, adding required special handling for
 * Slider entities.
 *
 * @ingroup slider
 */
class sliderStorage extends SqlContentEntityStorage implements sliderStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(sliderInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {slider_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {slider_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(sliderInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {slider_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('slider_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
