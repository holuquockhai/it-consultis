<?php

namespace Drupal\slider\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Slider entities.
 *
 * @ingroup slider
 */
interface sliderInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Slider name.
   *
   * @return string
   *   Name of the Slider.
   */
  public function getName();

  /**
   * Sets the Slider name.
   *
   * @param string $name
   *   The Slider name.
   *
   * @return \Drupal\slider\Entity\sliderInterface
   *   The called Slider entity.
   */
  public function setName($name);

  /**
   * Gets the Slider creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Slider.
   */
  public function getCreatedTime();

  /**
   * Sets the Slider creation timestamp.
   *
   * @param int $timestamp
   *   The Slider creation timestamp.
   *
   * @return \Drupal\slider\Entity\sliderInterface
   *   The called Slider entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Slider published status indicator.
   *
   * Unpublished Slider are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Slider is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Slider.
   *
   * @param bool $published
   *   TRUE to set this Slider to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\slider\Entity\sliderInterface
   *   The called Slider entity.
   */
  public function setPublished($published);

  /**
   * Gets the Slider revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Slider revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\slider\Entity\sliderInterface
   *   The called Slider entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Slider revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Slider revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\slider\Entity\sliderInterface
   *   The called Slider entity.
   */
  public function setRevisionUserId($uid);

}
