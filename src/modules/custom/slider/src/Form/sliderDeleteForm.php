<?php

namespace Drupal\slider\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slider entities.
 *
 * @ingroup slider
 */
class sliderDeleteForm extends ContentEntityDeleteForm {


}
