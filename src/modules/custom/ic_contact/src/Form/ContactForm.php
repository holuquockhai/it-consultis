<?php

namespace Drupal\ic_contact\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactForm.
 */
class ContactForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#theme'] = 'contact_form';
    $form['#attached'] = [
      'library' => [
        'ic_contact/contact-styling',
      ]
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['form-control'],
        'id'=>'name',
        'placeholder'=>'Your Name'
      ],
      '#required'=> TRUE
    ];
  
    $form['mail'] = [
      '#type' => 'email',
      '#attributes' => [
        'class' => ['form-control'],
        'id'=>'email',
        'placeholder'=>'Your Email'
      ],
      '#required'=> TRUE
    ];
  
    $form['subject'] = ['#type' => 'textfield',
      '#attributes' => [
        'class' => ['form-control'],
        'id'=>'subject',
        'placeholder'=>'Your Subject'
      ],
      '#required'=> TRUE
    ];
  
    $form['number'] = [
      '#type' => 'number',
      '#attributes' => [
        'class' => ['form-control'],
        'id'=>'number',
        'placeholder'=>'Your Number'
      ],
      '#required'=> TRUE
    ];
  
    $form['message'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'class' => ['form-control'],
        'id'=>'message',
        'placeholder'=>'Your Message'
      ],
      '#required'=> TRUE
    ];
  
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Join the team'),
    );
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SEND MESSAGE'),
      '#attributes' => [
        'class' => ['btn btn-default'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    drupal_set_message("Thanks for your feedback!");
  }

}
