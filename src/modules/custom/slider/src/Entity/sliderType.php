<?php

namespace Drupal\slider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Slider type entity.
 *
 * @ConfigEntityType(
 *   id = "slider_type",
 *   label = @Translation("Slider type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\slider\sliderTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\slider\Form\sliderTypeForm",
 *       "edit" = "Drupal\slider\Form\sliderTypeForm",
 *       "delete" = "Drupal\slider\Form\sliderTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\slider\sliderTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "slider_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "slider",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/slider_type/{slider_type}",
 *     "add-form" = "/admin/structure/slider_type/add",
 *     "edit-form" = "/admin/structure/slider_type/{slider_type}/edit",
 *     "delete-form" = "/admin/structure/slider_type/{slider_type}/delete",
 *     "collection" = "/admin/structure/slider_type"
 *   }
 * )
 */
class sliderType extends ConfigEntityBundleBase implements sliderTypeInterface {

  /**
   * The Slider type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Slider type label.
   *
   * @var string
   */
  protected $label;

}
