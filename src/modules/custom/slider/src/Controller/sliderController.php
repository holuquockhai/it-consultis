<?php

namespace Drupal\slider\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\slider\Entity\sliderInterface;

/**
 * Class sliderController.
 *
 *  Returns responses for Slider routes.
 */
class sliderController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Slider  revision.
   *
   * @param int $slider_revision
   *   The Slider  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($slider_revision) {
    $slider = $this->entityManager()->getStorage('slider')->loadRevision($slider_revision);
    $view_builder = $this->entityManager()->getViewBuilder('slider');

    return $view_builder->view($slider);
  }

  /**
   * Page title callback for a Slider  revision.
   *
   * @param int $slider_revision
   *   The Slider  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($slider_revision) {
    $slider = $this->entityManager()->getStorage('slider')->loadRevision($slider_revision);
    return $this->t('Revision of %title from %date', ['%title' => $slider->label(), '%date' => format_date($slider->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Slider .
   *
   * @param \Drupal\slider\Entity\sliderInterface $slider
   *   A Slider  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(sliderInterface $slider) {
    $account = $this->currentUser();
    $langcode = $slider->language()->getId();
    $langname = $slider->language()->getName();
    $languages = $slider->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $slider_storage = $this->entityManager()->getStorage('slider');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $slider->label()]) : $this->t('Revisions for %title', ['%title' => $slider->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all slider revisions") || $account->hasPermission('administer slider entities')));
    $delete_permission = (($account->hasPermission("delete all slider revisions") || $account->hasPermission('administer slider entities')));

    $rows = [];

    $vids = $slider_storage->revisionIds($slider);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\slider\sliderInterface $revision */
      $revision = $slider_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $slider->getRevisionId()) {
          $link = $this->l($date, new Url('entity.slider.revision', ['slider' => $slider->id(), 'slider_revision' => $vid]));
        }
        else {
          $link = $slider->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.slider.translation_revert', ['slider' => $slider->id(), 'slider_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.slider.revision_revert', ['slider' => $slider->id(), 'slider_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.slider.revision_delete', ['slider' => $slider->id(), 'slider_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['slider_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
