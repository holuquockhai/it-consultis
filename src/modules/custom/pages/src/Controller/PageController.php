<?php
/**
 * Created by PhpStorm.
 * User: holuquockhai
 * Date: 8/10/17
 * Time: 7:49 AM
 */
namespace Drupal\pages\Controller;
use Drupal\Core\Controller\ControllerBase;

class PageController extends ControllerBase{
 public function home(){
   return [
     '#theme' => 'landing_page',
     '#attached' => [
       'library' => [
         'pages/landingpage.blogs.ajax',
       ],
     ],
   ];
 }
}
