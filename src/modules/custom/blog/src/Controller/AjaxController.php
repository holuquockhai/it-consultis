<?php
/**
 * Created by PhpStorm.
 * User: holuquockhai
 * Date: 8/10/17
 * Time: 7:21 AM
 */
namespace Drupal\blog\Controller;
use Drupal\blog\BlogStorage;
use Drupal\Core\Controller\ControllerBase;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends ControllerBase{
  
  /**
   * @param $page
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function listBlog($page){
    $nodes = BlogStorage::ajax_load_blog($page);
    $data = [];
    foreach ($nodes as $node){
      //get image real url
      $image_url = ImageStyle::load('blog_thumb_400x237')->buildUrl($node->field_feature_image->entity->getFileUri());
      
      //get author
      $user = \Drupal\user\Entity\User::load($node->getOwnerId());
      $username = "N/A";
      if (!is_null($user)) {
        $username = $user->getDisplayName();
      }
      
      $data[] =[
        'title'=> word_trim($node->title->value,5),
        'node_url'=> "/node/".$node->nid->value,
        'img_url'=> $image_url,
        'comment_count'=>BlogStorage::countBlogComment($node->nid->value),
        'author'=>$username,
        'short_content'=>text_summary($node->body->value,null, 100)
      ];
    }
    
    return new JsonResponse($data);
  }
}