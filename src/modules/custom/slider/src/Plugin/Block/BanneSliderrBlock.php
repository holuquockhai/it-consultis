<?php
/**
 * Created by PhpStorm.
 * User: holuquockhai
 * Date: 8/8/17
 * Time: 8:08 AM
 */

namespace Drupal\slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slider\BannerSliderEntityQuery;
use Drupal\slider\Entity\slider;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "banner_slider",
 *   admin_label = @Translation("Banner Slider"),
 *   category = @Translation("Banner Slider"),
 * )
 */
class BanneSliderrBlock extends BlockBase {
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    // TODO: Implement build() method.
   
   $entities = $this->getListSlider();
    return
      [
        '#theme' => 'banner_slider',
        '#data'=> $entities,
        '#markup' => $this->t('Banner Slider block'),
      ];
  }
  
  /**
   * @return \Drupal\Core\Entity\EntityInterface[]
   * function get list slider
   */
  private function getListSlider(){
    $query = \Drupal::service('entity.query')
      ->get('slider')
      ->condition('status', TRUE);
    $entity_ids = $query->execute();
    $controller = \Drupal::entityManager()->getStorage('slider');
    $entities = $controller->loadMultiple($entity_ids);
    return $entities;
  }
  
}