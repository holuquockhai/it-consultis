<?php

namespace Drupal\blog;

/**
 * Class DbtngExampleStorage.
 */
class BlogStorage {
  
  /**
   * @param $page
   * @param int $limit
   * @param array $entry
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * get blogs for ajax purpose
   */
  public static function ajax_load_blog($page,$limit = 10, array $entry = array()) {
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'blog')
      ->range($page*$limit,$limit)
      ->sort('created' , 'DESC')
      ->execute();
    
    //load node multiple
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($nids);
    
    return $nodes;
  }
  
  /**
   * @param $nid
   *
   * @return int
   * function count comment of specific blog
   */
  public static function countBlogComment($nid){
    $cids = \Drupal::entityQuery('comment')
      ->condition('entity_id', $nid)
      ->condition('entity_type', 'blog')
    ->execute();
    
    return count($cids);
  }
  
  /**
   * @param $limit
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * function get list blogs and pagination by entity query
   */
  public static function getListBlogs($limit){
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'blog')
      ->sort('created', 'DESC')
      ->pager($limit);
    
    $nids = $query->execute();
    
    //load node multiple
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($nids);
    return $nodes;
  }
}
