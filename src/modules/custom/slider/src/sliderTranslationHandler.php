<?php

namespace Drupal\slider;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for slider.
 */
class sliderTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
