(function ($) {
  var page = 1;
  $(document).ready(function(){
    // var w = $(window);
    ajax_load_blogs(page);
    var w = $(window);
    w.scroll(function() {
      if ($(document).height() - w.height() == w.scrollTop()) {
        page++;
        ajax_load_blogs(page);
      }
    });

    function ajax_load_blogs(page) {
      $.post('/blogs/ajax/'+page, {}, function(data, textStatus, xhr) {
        var default_html = $("#infinite_scroll");
        if(data.length > 0){
          $.each(data, function(idx, obj){
            default_html.find(".news_title h4").text(obj.title);
            default_html.find(".news_title a").attr("href", obj.node_url);
            default_html.find(".single_latest_news_img_area img").attr("src", obj.img_url);
            default_html.find(".author p").text(obj.author);
            default_html.find(".comments p").text(obj.comment_count);
            default_html.find(".news_content p").text(obj.short_content);
            $('.cls-scroller').append(default_html.html());
          });
        }
      });
    }
  });
}(jQuery));


