<?php
namespace Drupal\pages\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *
 * @Block(
 *   id = "front_cool_fact",
 *   admin_label = @Translation("Front Cool Fact"),
 *   category = @Translation("Front Cool Fact"),
 * )
 */
class FrontCollFactBlock extends BlockBase {
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    // TODO: Implement build() method.
    return
      [
        '#theme' => 'front_cool_fact_block',
      ];
  }
  
}