<?php

namespace Drupal\slider;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\slider\Entity\sliderInterface;

/**
 * Defines the storage handler class for Slider entities.
 *
 * This extends the base storage class, adding required special handling for
 * Slider entities.
 *
 * @ingroup slider
 */
interface sliderStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Slider revision IDs for a specific Slider.
   *
   * @param \Drupal\slider\Entity\sliderInterface $entity
   *   The Slider entity.
   *
   * @return int[]
   *   Slider revision IDs (in ascending order).
   */
  public function revisionIds(sliderInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Slider author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Slider revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\slider\Entity\sliderInterface $entity
   *   The Slider entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(sliderInterface $entity);

  /**
   * Unsets the language for all Slider with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
