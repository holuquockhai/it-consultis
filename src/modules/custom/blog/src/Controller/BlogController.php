<?php
namespace Drupal\blog\Controller;

use Drupal\blog\BlogStorage;
use Drupal\Core\Controller\ControllerBase;

class BlogController extends ControllerBase{
  
  /**
   * @return array
   */
  public function content(){
    $nodes = BlogStorage::getListBlogs(20);
    $output = \Drupal::entityTypeManager()->getViewBuilder("node")->viewMultiple($nodes,"blog_list");
    $build['blog_list'] = [
      '#theme'=>'blogs_page_content',
      '#nodes'=>$output
    ];
    $build['pager'] = array(
      '#type' => 'pager',
      '#weight' => 10,
    );
    return $build;
  }
}