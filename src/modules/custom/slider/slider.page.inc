<?php

/**
 * @file
 * Contains slider.page.inc.
 *
 * Page callback for Slider entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Slider templates.
 *
 * Default template: slider.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_slider(array &$variables) {
  // Fetch slider Entity Object.
  $slider = $variables['elements']['#slider'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
